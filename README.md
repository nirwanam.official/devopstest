# DevOpsTest

OS Windows 10

# Soal 1
Dalam sebuah perusahaan Andi bekerja sebagai DevOps, jelaskan menurut pendapatmu definisi tentang DevOps dan seberapa penting DevOps di suatu perusahaan serta gambarkan flow DevOps dari development hingga ke production? 

# Jawab:
DevOps ialah sebuah team yang di bentuk untuk koordinasi antar team Developer dengan team Operational yang memberikan efek efisien dan efektif. **Sangat penting** memiliki Team DevOps dalam suatu perusahaan karena bertujuan untuk menambahkan efektifitas serta efisien yang terbentuk dari kolaborasi antar team.
hal-hal yang di hasilkan sebagai berikut:
1. Meningkatkan Deployment Frequency.
2. Meningkatkan Waktu Pemasaran.     
3. Mempersingkat Waktu Perbaikan.
4. Menurunkan Tingkat Kegagalan pada rilisan terbaru.
5. Meningkatkan Waktu Pemulihan.

Berikut ialah Flow DevOps:
![Link ke Gambar](/Images/01.png)

# Soal 2
Sebutkan perbedaan antara AWS dan OpenStack? Sebutkan secara jelas list tersebut!

# Jawab:
1. Computation.
    - AWS dengan EC2 yang merupakan Jaringan Virtual based on Big Data Analytics.
    - OpenStack membanggakan IaaS infrastruktur-nya.
2. Networking.
    - AWS memiliki DNS scalable rout 53, Amazon ELB, Amazon VPC dengan memperluas kemampuannya untuk terkoneksi dengan server-server perusahaan. dan AWS akan mengalokasikan alamat IP pribadi untuk instans yang berjalan di DHCP lalu ELB hanya akan membantu di Traffic yang menuju ke EC2.
    - OpenStack memiliki LBaas dan flat network VLAN secara otomatis ataupun secara manajemen manual, disini kita dapat membangun kekuatan untuk networking serta fungsi dari networking itu.
3. Security.
    - AWS menjadi pemenang dalam bidang ini, yang dimana AWS dapat me-render the desired array of services ketika dibutuhkan. sementara OpenStack masih belum memiliki fitur ini.
4. Storage.
    - AWS memiliki S3.
    - OpenStack memiliki Swift as Block Storage-nya dan Cinder, ABS sebagai mitra penyimpanan object-nya. 

# Soal 3
Tools apakah yang dapat digunakan untuk mempercepat proses deployment aplikasi tanpa harus setup server manual? Sebutkan dan jelaskan! 

# Jawab:
1. **ManageEngine Dekstop Central** 
  - Software Repository dimana semua bundle installasi tersimpan.
  - Otomatis dalam pengecekan System Requirements pada setiap perangkat.
  - Administrator System dapat membuat Wizard installasi untuk user menginstall secara On Demand.
  - Memiliki Status Report.  

2. **Atlassian Bamboo**
    - CI/CD yang dikembangkan oleh Atlassian, digunakan untuk mengotomatisasi manajemen rilis aplikasi perangkat lunak.
    - Kelebihan: Beberapa keuntungan dari bamboo dibandingkan orang-orang sesamanya seperti Jenkins adalah bahwa ia memiliki alur kerja percabangan Git dan proyek-proyek penempatan yang terintegrasi. Ini juga memiliki integrasi built-in dengan perangkat lunak Atlassian lainnya seperti Jira , Confluence , Bitbucket , HipChat, dll, yang juga banyak digunakan.
    - Kekurangan: Berbayar dan bukan Open Source.

3. **Octopus Deploy**
    - Software ini dimaksudkan untuk merilis software yang di produksi tetapi dapat juga digunakan untuk Software Package yang dibeli. memiliki Library of templates termasuk system variable, yang membuat software ini mudah beradaptasi (Highly Adaptable).
    - memiliki Release Management tools.
    - Cloud Service.
    - Octopus Cloud and Server dapat digunakan secara Gratis hingga 10 Target Deployment, dan dikenakan biaya lebih dari itu untuk Cloud dibayarkan per Bulan, dan Server-nya per Tahun.

# Soal 4
CI/CD (Continuous Integration/Continuous Development) merupakan hal yang sering ditemui oleh seorang DevOps. Menurut pendapatmu mengapa CI/CD diperlukan dan berikan gambaran diagram kerjanya (Workflow)! 

# Jawab:
- CI merupakan pengecekan merge yang akan dilakukan secara otomatis tiap mergenya. Sehingga ketika melakukan merge request, akan dicek apakah branch itu sudah memenuhi syarat? baik dari segi test, linter, dan build. Setelah itu juga akan dicek apakah diperlukan mengatasi merge konflik. Dan hal ini tidak akan membebankan Developer.

- CD akan memastikan secara otomatis bahwa release siap dipakai dan sudah lulus acceptance test atau siap dideploy. Sehingga hasil proses otomatis dan cepat yang dihasilkan, merupakan sebuah release yang siap apabila hendak dilakukan deploy. Perbedaan utama dengan CI adalah, hasil dari CI merupakan produk yang dapat dijalankan tanpa adanya error. Sedangkan CD ini menghasilkan produk yang sudah dapat dilakukan release secara otomatis.

![Link ke Gambar CI/CD Work Flow](/Images/04-CICD-WorkFlow.png)

# Soal 5
Apa perbedaan puppet, ansible, chef dan terraform? Gambarkan dan jelaskan!

# Jawab:
| Point Perbedaan | Ansible | Puppet | Terraform |
| ------ | ------ | ------ | ------ |
| Management and Scheduling | Penyebaran secara instant, karena server mendorong konfigurasi ke node. ketika sudah jadwalnya, Ansible Tower versi Enterprise dapat membackup ketika versi Free tidak ada | berfokus terutama pada konfigurasi push and pulls, di mana klien menarik konfigurasi dari server. Konfigurasi harus ditulis dalam Puppet Language. Ketika sudah jadwalnya, pengaturan default Puppet memungkinkannya untuk memeriksa semua node untuk melihat apakah mereka berada dalam keadaan yang diinginkan. | sumber daya scheduler berfungsi sama seperti penyedia yang memungkinkannya untuk meminta sumber daya dari mereka. Dengan demikian, itu hanya tidak terbatas pada penyedia fisik seperti AWS, memungkinkan penggunaannya dalam lapisan. Terraform dapat digunakan untuk menyediakan ke grid terjadwal, serta mengatur infrastruktur fisik yang menjalankan scheduler. |
| Ease of Setup and Use | Ansible lebih mudah dipasang dan digunakan. Ini memiliki master tanpa agen, berjalan pada mesin klien. Fakta bahwa itu tanpa agen berkontribusi secara signifikan terhadap kesederhanaannya. Ansible menggunakan sintaks YAML, ditulis dalam bahasa Python, yang hadir bawaan sebagian besar penyebaran Linux dan Unix. | lebih berbasis model, dimaksudkan untuk administrator sistem. Server Puppet dapat diinstal pada satu atau beberapa server, sementara agen Puppet memerlukan instalasi pada semua node yang memerlukan manajemen. Model ini adalah model klien-server atau agen-master. Waktu pemasangan dapat memakan waktu sekitar sepuluh hingga tiga puluh menit. | juga lebih sederhana untuk dipahami ketika datang ke pengaturan serta penggunaannya. Bahkan memungkinkan pengguna untuk menggunakan proxy server jika diperlukan untuk menjalankan penginstal. |
| Availability: | Ansible memiliki node sekunder jika node aktif mengalami down. | Puppet memiliki satu atau lebih master, jika dalam kasus master originalnya down. | Tidak dapat di aplikasikan pada terraform. |
| Scalability: | Scalability lebih mudah dicapai | Scalability kurang mudah dicapai | Scalability relatif mudah dicapai |

# Soal 6
Bagaimana cara kerja HTTP dan Load Balance sebuah aplikasi? Gambarkan dan jelaskan! 

# Jawab:
 - Cara kerja HTTP dapat dijabarkan sebagai berikut :
    1. Pertama-tama, komputer klien (HTTP klien) membuat sambungan, lalu mengirimkan permintaan dokumen ke web server.
    2. HTTP server kemudian memproses permintaan klien, sementara itu, HTTP klien menunggu respon dari server tersebut.
    3. Web server merespon permintaan dengan kode status data, lalu barulah menutup sambungan ketika telah selesai memproses permintaan.
![Link Gambar Cara Kerja HTTP](/Images/06-HTTP.png)

- Cara kerja Load Balance:
Algoritma load balancing paling sederhana yaitu membagi beban secara bergiliran dan berurutan dari satu server ke server lain. Algoritma ini membagi beban dengan cara memberi rasio pada setiap servernya semakin besar rasio maka semakin besar juga server tersebut menangani beban.
![Link Gambar Cara Kerja Load Balance](/Images/06-Load_Balance.png)

# Soal 7
Gambarkan sebuah infrastruktur server yang baik dan benar untuk aplikasi microservices? Sertakan gambar!

# Jawab:
masing – masing microservice seperti file service, user service, content service dan transaction service memiliki basis datanya masing-masing untuk mendukung modularity serta menjamin single responsibility dari tiap
microservice. user-service akan bertugas untuk melakukan pengolahan data terkait dengan user. Microservice
content-service bertugas untuk melakukan pengolahan data. Microservice file-service bertugas untuk mengolah data
terkait file. microservice transaction-service bertugas untuk mengolah data terkait
dengan transaksi. Lalu menggunakan REST API (Representational State Transfer Application
Programming Interface) masing-masing microservice mengekspos routes yang dapat diakses
oleh internal microservice lain atau layer lain di dalam sistem secara keseluruhan.
Komponen selanjutnya  browser request. Komponen ini bertugas sebagai akses
untuk client agar dapat mengakses sistem.
![Link Gambar Cara Kerja Load Balance](/Images/07-Infrastruktur-server-microservice-applikasi.png)

# Soal 8
Deploy sebuah aplikasi wordpress dengan database MySQL / Postgres menggunakan heroku cli, tidak boleh melalui dashboard heroku. Buatlah step by step-nya secara terstruktur. Dan pastikan aplikasinya dapat di akses secara publik! Sertakan screenshot step by step-nya atau record menjadi sebuah video.

# Jawab:
 -
